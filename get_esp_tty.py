#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Determine tty for ESP board connected.

ESP8266
ESP32
"""

import sys

import esptool

__date__ = "2020/07/12 00:46:35 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

TTYS = (
    "/dev/ttyUSB0",
    "/dev/ttyUSB1",
    "/dev/ttyUSB2",
    "/dev/ttyUSB3",
    "/dev/ttyUSB4",
    "/dev/ttyUSB5",
)
for TTY in TTYS:
    STDOUT = sys.stdout
    sys.stdout = open("/dev/null", "w")
    esp = esptool.ESPLoader.detect_chip(TTY, esptool.ESPLoader.ESP_ROM_BAUD)
    sys.stdout = STDOUT
    if esp.CHIP_NAME == sys.argv[1]:
        print(TTY)
        raise SystemExit(0)

raise SystemExit("System not found.")

# Local Variables:
# mode: python
# compile-command: "poetry run tox"
# time-stamp-pattern: "30/__date__ = \"%:y/%02m/%02d %02H:%02M:%02S %u\""
# End:
