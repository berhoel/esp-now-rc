# ESP-Now-RC

Attempt to create an ESP-Now based remove controller.

```plantuml
start
fork
  partition ESP32 {
    :intialize sender;
    while (poweron)
      :read joystick position;
      :send read joystick values;
    endwhile (poweroff)
  }
fork again
  partition ESP8266 {
    :intialize receiver;
    while (poweron)  
      :receive joystiock position;
      :set servos acc. to joystick position;
    endwhile (poweroff)
  }
end fork
stop
```

```plantuml
participant Joystick order 0
ref over ESP32, ESP8266
  initializattion
end ref
loop
Joystick->ESP32 : Resistance
ESP32->ESP8266 : ESP-Now
ESP8266->Servo : PWM
end
```
