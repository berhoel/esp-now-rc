# -*- coding: utf-8 -*-

# Copyright © 2020 by Berthold Höllmann

# Build ESP32/8266 based RC.

# ID: $Id$
# $Date$
# $Revision$
# Author Berthold Höllmann <berhoel@gmail.com>

SHELL = /bin/sh

TTY_ESP32 = /dev/ttyUSB_CC50E39C1608
TTY_ESP8266_1 = /dev/ttyUSB_A4CF12DE7011
TTY_ESP8266_2 = /dev/ttyUSB_A4CF12DD5627

ARCH_ESP32 = "esp32:esp32:esp32"
ARCH_ESP8266 = "esp8266:esp8266:nodemcuv2"


all: Sender_ Receiver_
# ESP32_Get_MAC_Address_ ESP8266_Get_MAC_Address_

Sender_: Sender/system.h
	arduino-cli compile --fqbn $(ARCH_ESP32) Sender
	arduino-cli upload -p $(TTY_ESP32) --fqbn $(ARCH_ESP32) Sender

Sender_ESP8266_: Sender_ESP8266/system.h
	arduino-cli compile --fqbn $(ARCH_ESP8266) Sender_ESP8266
	arduino-cli upload -p $(TTY_ESP32) --fqbn $(ARCH_ESP8266) Sender_ESP8266

Receiver_: Receiver/system.h
	arduino-cli compile --fqbn $(ARCH_ESP8266) Receiver
	arduino-cli upload -p $(TTY_ESP8266_1) --fqbn $(ARCH_ESP8266) Receiver
	arduino-cli upload -p $(TTY_ESP8266_2) --fqbn $(ARCH_ESP8266) Receiver

ESP32_Get_MAC_Address_:
	arduino-cli compile --fqbn $(ARCH_ESP32) ESP32_Get_MAC_Address
	arduino-cli upload -p $(TTY_ESP32) --fqbn $(ARCH_ESP32) ESP32_Get_MAC_Address
	cu -l $(TTY_ESP32) -s 115200

ESP8266_Get_MAC_Address_:
	arduino-cli compile --fqbn $(ARCH_ESP8266) ESP8266_Get_MAC_Address
	arduino-cli upload -p $(TTY_ESP8266_1) --fqbn $(ARCH_ESP8266) ESP8266_Get_MAC_Address
	-cu -l $(TTY_ESP8266_1) -s 115200
	arduino-cli upload -p $(TTY_ESP8266_2) --fqbn $(ARCH_ESP8266) ESP8266_Get_MAC_Address
	cu -l $(TTY_ESP8266_2) -s 115200

%/system.h: system.h
	ln -s ../$< $@

term_ESP32:
	cu -l $(TTY_ESP32) -s 115200

term_ESP8266_1:
	cu -l $(TTY_ESP8266_1) -s 115200

term_ESP8266_1:
	cu -l $(TTY_ESP8266_2) -s 115200

# Local Variables:
# mode: makefile
# compile-command: "make"
# End:


