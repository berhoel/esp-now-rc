/**
   \file
   \author Berthold Höllmann <berhoel@gmail.com>
   \copyright Copyright © 2020 by Berthold Höllmann
   \brief System settings for the project.

   Detailed description
*/

#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#define MAC_ESP32                                                              \
  { 0xCC, 0x50, 0xE3, 0x9C, 0x16, 0x08 }
#define MAC_ESP8266_1                                                          \
  { 0xA4, 0xCF, 0x12, 0xDE, 0x70, 0x11 }
#define MAC_ESP8266_2                                                          \
  { 0xA4, 0xCF, 0x12, 0xDD, 0x56, 0x27 }

typedef struct message {
  int axis1;
  int axis2;
  bool button1;
} message;

#endif // _SYSTEM_H_

/*
  Local Variables:
  mode: c
  c-file-style: "gcc"
  indent-tabs-mode: nil
  compile-command: "make"
  coding: utf-8
  End:
 */
