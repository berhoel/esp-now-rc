/**
 * ESP-NOW based RC controller (ESP32).
 *
 * based on <https://www.survivingwithandroid.com/esp-now-esp32-esp8266/>
 */

#include <Arduino.h>
#include <WiFi.h>
#include <esp_now.h>

#include "system.h"

// ESP8266 Mac address (first controller)
uint8_t mac_receiver1[] = MAC_ESP8266_1;
// ESP8266 Mac address (second controller)
uint8_t mac_receiver2[] = MAC_ESP8266_2;

esp_now_peer_info_t receiver1;
esp_now_peer_info_t receiver2;

int i = 0;

// Joystick button is connected to GPIO5
const int joyB = 5;
// Joystick X-axis connected to GPIO12
const int joyX = 34;
// Joystick X-axis connected to GPIO12
const int joyY = 35;

struct message myMessage;

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);

  // Get Mac Add
  Serial.print("Mac Address: ");
  Serial.print(WiFi.macAddress());
  Serial.println("ESP32 ESP-Now Broadcast");

  // Initializing the ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Problem during ESP-NOW init");
    return;
  }

  memcpy(receiver1.peer_addr, mac_receiver1, 6);
  receiver1.channel = 4;
  receiver1.encrypt = 0;

  // Register the receiver
  Serial.println("Registering a receiver 1");
  if (esp_now_add_peer(&receiver1) == ESP_OK) {
    Serial.println("Receiver 1 added");
  }

  memcpy(receiver2.peer_addr, mac_receiver2, 6);
  receiver2.channel = 4;
  receiver2.encrypt = 0;

  // Register the receiver
  Serial.println("Registering a receiver 2");
  if (esp_now_add_peer(&receiver2) == ESP_OK) {
    Serial.println("Receiver 2 added");
  }

  // Setup Joystick button as digital in.
  pinMode(joyB, INPUT);
}

void loop() {
  myMessage.axis1 = abs(analogRead(joyX) * 180 / 4095);
  myMessage.axis2 = abs(analogRead(joyY) * 180 / 4095);
  myMessage.button1 = digitalRead(joyB);
  esp_now_send(NULL, (uint8_t *)&myMessage, sizeof(myMessage));
}
