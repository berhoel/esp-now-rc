/**
 * ESP-NOW based RC receiver (ESP8266).
 *
 * based on <https://www.survivingwithandroid.com/esp-now-esp32-esp8266/>
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <espnow.h>

#include <Servo.h>

#include "system.h"

Servo servo1;
message myMessage;

void onDataReceiver(uint8_t *mac, uint8_t *incomingData, uint8_t len) {
  // Serial.println("Message received.");
  // We don't use mac to verify the sender
  // Let us transform the incomingData into our message structure
  memcpy(&myMessage, incomingData, sizeof(myMessage));
  // Serial.println("Message interpreted.");
}

void setup() {
  Serial.begin(115200);
  WiFi.disconnect();
  ESP.eraseConfig();

  // Wifi STA Mode
  WiFi.mode(WIFI_STA);
  // Get Mac Add
  Serial.print("Mac Address: ");
  Serial.print(WiFi.macAddress());
  Serial.println("\nESP-Now Receiver");

  // Initializing the ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Problem during ESP-NOW init");
    return;
  }

  // esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);
  // We can register the receiver callback function
  esp_now_register_recv_cb(onDataReceiver);

  // Servo setup
  servo1.attach(D4);
  servo1.write(0);
}

void loop() {
  // put your main code here, to run repeatedly:
  servo1.write(min(abs(myMessage.axis1), 180));
}
